# NOTE
**My projects are active only on GitHub, these are only the initial releases on BitBucket. GitHub: https://github.com/areeb-beigh**

# Python RSS2IRC Bot
This is a simple IRC bot that'll return feeds from an RSS source to an IRC channel.
I've used [this rss2irc](https://github.com/maK-/rss2irc-bot) bot features for the basic functionality and made many additions,
bug fixes and improvisations.

## Configuration
To configure the bot open rss2irc.py with any text editor, scroll down to the config part and replace the default info with the desired one.
Description for each feild is marked after the "#" hastag.

## Commands
* !feed                 -        Returns last 3 feeds
* !feed last (1-4)      -        Returns last 'n' number of feeds
* !feed list            -        Returns the feed list currently being used
* !credits              -        View bot credits
* !feed help            -        View this help dialogue

## Possible Snags
##### 1. Bot is not joining the channel
Once the script is started the bot will take sometime to login and join the channel (minimum 40 seconds),
yet it depends on the latency. You can do a /whois <bot nick> to check if the bot has at least connected to the network.
If it does not join try restarting the script.

##### 2. No feeds
Check if the URL you provided is working and it is an RSS feed. 
[here's what an RSS page looks like](http://www.irchound.tk/forum/syndication.php?fid=2,14,18,4,5,11,17,6,21,23,24,22&limit=5).

##### 3. Bot is not logging in
This bot uses NickServ to login and which will happen only if the nick name it uses is regsitered. Make sure
the nickname is registered and the password you have entered is correct. If it still does not work the network you're connecting to
probably does not use NickServ.

##### 4. Bugs
If you find any bugs / miss typed stuff in the code please feel free to make a pull request / open an issue on this repo.
You can also contact me [here](http://www.areeb-beigh.tk/contact.html).

Cheers!

* **Developer**: Areeb Beigh
* **Website**: [www.areeb-beigh.tk](www.areeb-beigh.tk)
* **Mail:** areebbeigh@gmail.com
* **Version**: 2.0